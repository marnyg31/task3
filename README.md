Solution for task 3

After running the program, you will be prompted to enter a search term.
All items in the list containing the search term will then be presented.
The user will then be asked if he would like to exit or try again

The relevant code is found in /task3/Program.cs
